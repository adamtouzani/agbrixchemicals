<?php

ini_set('session.cookie_lifetime', 60 * 60 * 72);
ini_set('session.gc-maxlifetime', 60 * 60 * 72);
session_start();
$time = $_SERVER['REQUEST_TIME'];


if (!isset($_SESSION['last_active']) || (isset($_SESSION['last_active']) && ($time - $_SESSION['last_active']) > $_SESSION['timeout_duration'])) {
    session_unset();
    session_destroy();
    header('Location: .');
    die();
}

$_SESSION['last_active'] = $time;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="AG Brix Chemicals Database">
    <meta name="author" content="Adam Touzani">
    <title>AG Brix Chemicals</title>
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="vendor/font-awesome/css/all.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="">AG Brix Chemical Database</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="chemicalLocations">
            <li class="nav-item">
                <a class="nav-link" href="allChemicals" id="all_chemicals_button" data-toggle="tooltip" data-placement="left" title="All Chemicals">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">All Chemicals</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="poisonBox" id="poison_box_button" data-toggle="tooltip" data-placement="right" title="Poison Box">
                    <i class="fas fa-fw fa-skull-crossbones"></i>
                    <span class="nav-link-text">Poison Box</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="chemicalsCabinet" id="cc_button" data-toggle="tooltip" data-placement="right" title="Chemical Cabinet">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">Chemical Cabinet</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="fridge2" id="fridge_2_button" data-toggle="tooltip" data-placement="right" title="Fridge 2">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">Fridge 2</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="freezer2" id="freezer_2_button" data-toggle="tooltip" data-placement="right" title="Freezer 2">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">Freezer 2</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="h1c" id="fh1_button" data-toggle="tooltip" data-placement="right" title="Fume Hood 1">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">Fume Hood 1</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="chemicalRoom" id="chemical_room_button" data-toggle="tooltip" data-placement="right" title="Chemical Room">
                    <i class="fas fa-fw fa-flask"></i>
                    <span class="nav-link-text">Chemical Room</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fas fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="logout.php">
                    <i class="fas fa-fw fa-sign-out-alt"></i>Logout</a>
            </li>
        </ul>
    </div>
</nav>
<div class="content-wrapper">
    <div class="container-fluid" id="content">
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © AG Brix Chemicals 2017</small>
            </div>
        </div>
    </footer>
    <!-- Modal for PDF Viewing -->
    <?php include 'views/pdf_viewing_modal.php'; ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.js"></script>
    <!-- Page Handler JS -->
    <script src="js/controller.js" type="module"></script>
</div>
</body>

</html>
