import { pdf_modal_handler } from './showPDF.js';

$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "controllers/allChemicals.php",
        success: function(result,status,xhr){
            $("#content").html(result);
        },
        error: function(result,status,xhr){
            swal("Uh oh!", "There was an error connecting to the server. Try again later.", "error");
            console.log(status);
            console.log(result);
        }
    });
});

$("#content").on('click', 'a', (e) => { pdf_modal_handler(e); });

$("#chemicalLocations").on('click', 'a', (event) => {
    event.preventDefault();
    $.ajax({
        method: "GET",
        url: "controllers/" + $(event.currentTarget).attr('href') + ".php",
        success: function(result,status,xhr){
            $("#content").fadeOut(200, () => {
                $("#content").html(result);
                $("#content").fadeIn(500);
            });
        },
        error: function(result,status,xhr){
            if(result.status === 403) {
                location.reload();
            }
            swal("Uh oh!", "There was an error connecting to the server. Try again later.", "error");
            console.log(status);
            console.log(result);
        }
    });
    return false;
});