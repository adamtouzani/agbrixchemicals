import { pdf_modal_handler } from './showPDF.js';

const resetModal = () => {
    $("#add_chemical_modal").modal('hide');
    $("#add_chemical_form").trigger('reset');
    $("#add_chemical_form .custom-file-label").html("Choose file (only PDF!)");
    $("#other_location").hide();
    $(".safety-statements-other-input").hide();
    $("#add_chemical_modal .modal-body").scrollTop(0);
};

const getHomepage = () => {
    $.ajax({
        method: "GET",
        url: "controllers/allChemicalsAdmin.php",
        success: function(result, status, xhr) {
            $("#content").html(result);
        },
        error: function(result, status, xhr) {
            swal("Uh oh!", "There was an error connecting to the server. Try again later.", "error");
            console.log(status);
            console.log(result);
        }
    });
};

$(document).ready(function() {
    $("#other_location").hide();
    $(".safety-statements-other-input").hide();
    getHomepage();
});

$("#content").on("click", '.sds_button, .sop_button', (e) => { pdf_modal_handler(e); });

$("#add_chemical_button").on("click", (e) => {
    e.preventDefault();
    $("#add_chemical_modal").modal('show');
    return false;
});

$("#add_chemical").on('click', (e) => {
    e.preventDefault();
    const wrapper = document.createElement('div');
    wrapper.innerHTML = '<div class="spinner"></div>';
    swal({
        text: "Adding Chemical",
        content: wrapper,
        buttons: false,
        closeOnClickOutside: false
    });

    const files = $('#add_chemical_form :file');
    const total_files = files[0].files.length + files[1].files.length;
    if (total_files !== 2) {
        swal("You have missing data in the form.", "The form is missing the SDS files, in English and/or German." +
            " Please make sure to select those files before adding the chemical to the DB.", "error");
        return;
    }

    for (const fileElement of files){
        if(fileElement.files[0].type !== 'application/pdf') {
            swal("You can only upload PDF files with this form.", "Make sure that both SDS files are in PDF format." +
                " If not, convert the file to PDF before uploading the file here.", "error");
            return;
        }
    }

    let totalSize = 0;
    for (const fileElement of files){
        console.log(fileElement.files[0]);
        totalSize += fileElement.files[0].size;
    }
    if (totalSize > 8300000) {
        swal("Your SDS PDF files are too large", "You have exceeded the file size limit of 8.3 MB." +
            " Try again with reduced-sized PDFs.", "error");
        return;
    }

    $.ajax({
        method: "POST",
        url: "controllers/addChemical.php",
        processData: false,
        contentType: false,
        data: new FormData($('#add_chemical_form')[0]),
        success: (res, status, xhr) => {
            const _response = $.parseJSON(res);
            console.log(_response.post, _response.query);
            swal(_response.message, _response.details, "success").then(() => {
                resetModal();
                getHomepage();
            });
        },
        error : (err, status, xhr) => {
            if(err.status === 403) {
                location.reload();
            }
            console.log(err, status, xhr);
            const _error = $.parseJSON(err.responseText);
            swal(_error.error, _error.message, "error");
        }
    })
});

$("#add_chemical_modal").on('click', '.close_modal', () => {
    swal({
        title: "Are you sure you want to cancel adding a new chemical to the DB?",
        text: "All the entered data will be lost.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willClose) => {
        if (willClose) {
            resetModal();
        }
    });
});

$("input[name=location]").on('change', function(){
    if(this.value=="other"){
        $("#other_location").show(400);
    } else {
        $("#other_location").hide(400);
    }
});

$("select").on('change', function(){
    if($(this).val().indexOf("other") >= 0){
        $("#other_"+$(this).attr('name').split("_")[1]+"_"+$(this).attr('name').split("_")[2]+"_text").show(500)
    } else {
        $("#other_"+$(this).attr('name').split("_")[1]+"_"+$(this).attr('name').split("_")[2]+"_text").hide(500)
    }
});

$('.custom-file-input').on('change', function() {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').html(fileName);
});