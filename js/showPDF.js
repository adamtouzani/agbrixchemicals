export function pdf_modal_handler(e) {
    e.preventDefault();
    let doc_type = $(e.currentTarget).attr('id').split('_')[0].toUpperCase();
    let chemical_id = $(e.currentTarget).attr('id').split('_')[2];
    let lang = $(e.currentTarget).attr('id').split('_')[1].toUpperCase();
    $("#pdf_embed_element").attr('src', doc_type + "/" + lang + "/" + chemical_id + ".pdf");
    $("#chemical_name_modal").html($("#chemical_" + chemical_id).text());
    $("#download_as_pdf").attr('href', doc_type + "/" + lang + "/" + chemical_id + ".pdf").attr('download', $("#chemical_" + chemical_id).text() + "_" + lang + ".pdf");
    $("#pdf_modal").modal('show');
    return false;
}