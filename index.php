<?php

require '../connectvars.php';
ini_set('session.cookie_lifetime', 60 * 60 * 72);
ini_set('session.gc-maxlifetime', 60 * 60 * 72);
session_start();
$time = $_SERVER['REQUEST_TIME'];

$error_message = '';
$style_message = '';


//If logging in
if ($_SERVER['REQUEST_METHOD'] === "POST") {
  //Get username and password
  $username = $_POST['username'];
  $password = $_POST['password'];
  //Check data with database
  $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
  if(!$db_connection){
    echo "error connecting to DB!";
    die(mysqli_connect_error());
  }
  mysqli_set_charset ($db_connection, "utf8");

  $query = "SELECT password, userType FROM users WHERE username = '$username'";
  $result = mysqli_query($db_connection, $query);

  if (mysqli_num_rows($result) > 0) {
    //Check if password matches
    while($row = mysqli_fetch_assoc($result)){
      if($row['password']==$password){
        $_SESSION['last_active'] = $time;
        $_SESSION['timeout_duration'] = $_POST['checkbox']=="remember-me" ? 172800 : ($row['userType'] == "admin" ? 1800 : 900);
        $_SESSION['userType'] = $row['userType'];
        $_SESSION['userType'] == "admin" ? header('Location: chemicalDBadmin.php') : header('Location: chemicalDB.php');
        die();
      } else {
        $error_message = '<div class="text-danger text-center" role="alert">Username and/or password incorrect.<br />Try again.</div><br />';
        $style_message = 'border-danger';
      }
    }
  } else {
    $error_message = '<div class="alert text-danger text-center" role="alert">Username and/or password incorrect.<br />Try again.</div><br />';
    $style_message = 'border-danger';
  }
}

//Check if logged in already
if (isset($_SESSION['last_active']) && ($time - $_SESSION['last_active']) < $_SESSION['timeout_duration']) {
    if($_SESSION['userType'] == "admin"){
      header('Location: chemicalDBadmin.php');
      die();
    }
    header('Location: chemicalDB.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Adam Touzani">

    <title>AG Brix Chemicals Database</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

  </head>

  <body class="bg-light">
    <main>
      <div class="container">
        <div class="card card-login <?php echo $style_message; ?> mx-auto mt-5">
          <div class="card-header text-center"><h3>AG Brix Chemicals</h3></div>
          <div class="card-body">
            <form class="form-signin" action="." method="post">
              <h5 class="form-signin-heading">Access Restricted</h5>
              <h6 class="form-signin-heading">Please sign-in to continue:</h6>
              <?php echo $error_message; ?>
              <label for="username" class="sr-only">Username</label>
              <input type="text" id="username" class="form-control" placeholder="Username" name="username" required>
              <label for="password" class="sr-only">Password</label>
              <input type="password" id="password" class="form-control" placeholder="Password" name="password" required>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me" name="checkbox"> Remember me
                </label>
              </div>
              <div><small>By logging in and accessing the database, you agree to the Terms of Use and Privacy Policy accessible from the link at the bottom of the page.</small></div>
              <br />
              <button class="btn btn-primary btn-block" type="submit">Access Database</button>
            </form>
          </div>
        </div>
      </div>
    </main>

    <footer class="footer">
      <div class="container">
        <span class="text-muted"><a href="#" data-toggle="modal" data-target="#privacy_policy_modal">Terms of Use/Privacy Policy</a></span>
      </div>
    </footer>
    <!-- Privacy Policy Modal -->
    <?php include 'views/privacy_policy_modal.php' ?>
    <!-- Core plugin JavaScript-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>
