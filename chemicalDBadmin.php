<?php

ini_set('session.cookie_lifetime', 60 * 60 * 72);
ini_set('session.gc-maxlifetime', 60 * 60 * 72);
session_start();

$time = $_SERVER['REQUEST_TIME'];

//Check if not logged in
if (!isset($_SESSION['last_active']) || (isset($_SESSION['last_active']) && ($time - $_SESSION['last_active']) > $_SESSION['timeout_duration'])) {
    session_unset();
    session_destroy();
    header('Location: .');
    die();
}

//Determine privileges
if ($_SESSION['userType']!="admin") {
    header('Location: chemicalDB.php');
    die();
}

$_SESSION['last_active'] = $time;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="AG Brix Chemicals Database">
    <meta name="author" content="Adam Touzani">
    <title>AG Brix Chemicals</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/font-awesome/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="css/spinner.css">
</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="mainNav">
        <a class="navbar-brand" href="">AG Brix Chemical Database – Admin</div></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn btn-success" href="controllers/allChemicalsSpecial.php" target="_blank">Gefahrstoffkataster</a>
                </li>
                <!-- <li class="nav-item" style="margin-left: 10px;">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for chemical...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">
                        <i class="fas fa-fw fa-sign-out-alt"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper" style="margin-left: 0px;">
        <br />
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <h3 style="text-align: center;">WG Brix Chemicals</h3>
                <h3 style="text-align: center;">Chemikalien AG Brix</h3>
            </div>
            <div class="col-md-4">
                <div class="float-right" style="margin-right: 30px;"><a href="#" class="btn btn-primary" id="add_chemical_button">Add new chemical</a></div>
            </div>
        </div>
        <br />
        <div class="container-fluid" id="content">
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
        <footer class="sticky-footer" style="width: 100% !important;">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © AG Brix Chemicals 2017</small>
                </div>
            </div>
        </footer>
        <!-- Modal for PDF Viewing -->
        <?php include 'views/pdf_viewing_modal.php' ?>
        <!-- Modal for Adding Chemicals -->
        <?php include 'views/new_chemical_modal.php'; ?>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->
        <script src="vendor/datatables/jquery.dataTables.js"></script>
        <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
        <!-- SweetAlert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin.min.js"></script>
        <!-- Custom scripts for this page-->
        <script src="js/sb-admin-datatables.min.js"></script>
        <!-- <script src="js/sb-admin-charts.min.js"></script> -->
        <!-- Page Handler JS -->
        <script src="js/controllerAdmin.js" type="module"></script>
    </div>
</body>

</html>
