<div class="modal fade" tabindex="-1" role="dialog" id="add_chemical_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document" style="min-height: 70%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add new Chemical to DB</h5>
                <button type="button" class="close close_modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 100%" id="add_chemical_modal_body">
                <div style="height: 100%">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="container">
                                <form id="add_chemical_form" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label for="official_name_DE" class="col-sm-4 col-form-label"><b>Chemical Name:</b></label>
                                        <input type="text" name="official_name_EN" class="form-control col-sm-6" placeholder="Chemical Name, English" id="official_name_EN">
                                    </div>
                                    <div class="form-group row">
                                        <label for="official_name_DE" class="col-sm-4 col-form-label"><b>Chemical Name (in German):</b></label>
                                        <input type="text" name="official_name_DE" class="form-control col-sm-6" placeholder="Chemical Name, German" id="official_name_DE">
                                    </div>
                                    <div class="form-group row">
                                        <label for="company" class="col-sm-4 col-form-label"><b>Company:</b></label>
                                        <input type="text" name="company" class="form-control col-sm-6" placeholder="Company" id="company">
                                    </div>
                                    <div class="form-group row">
                                        <label for="article_number" class="col-sm-4 col-form-label"><b>Article Number:</b></label>
                                        <input type="text" name="article_number" class="form-control col-sm-6" placeholder="Article Number" id="article_number">
                                    </div>
                                    <div class="form-group row">
                                        <label for="CAS_number" class="col-sm-4 col-form-label"><b>CAS Number:</b></label>
                                        <input type="text" name="CAS_number" class="form-control col-sm-6" placeholder="CAS Number" id="CAS_number">
                                    </div>
                                    <div class="form-group row">
                                        <label for="weight_quantity" class="col-sm-4 col-form-label"><b>Quantity & Weight:</b></label>
                                        <input type="text" name="weight_quantity" class="form-control col-sm-6" placeholder="Quantity/Weight" id="weight_quantity">
                                    </div>
                                    <hr />
                                    <div class="form-group">
                                        <label for="GHS_pictogram_codes"><b>GHS Pictograms (Select all that apply)</b></label><br />
                                        <small>If no pictograms apply, just skip this section.</small>
                                        <table style="text-align: center; margin-top: 10px;">
                                            <tr>
                                                <td><img src="GHS_symbols/GHS01.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS02.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS03.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS04.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS05.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS06.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS07.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS08.svg" alt="" width="70"></td>
                                                <td><img src="GHS_symbols/GHS09.svg" alt="" width="70"></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS01" value="GHS01"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS02" value="GHS02"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS03" value="GHS03"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS04" value="GHS04"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS05" value="GHS05"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS06" value="GHS06"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS07" value="GHS07"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS08" value="GHS08"></td>
                                                <td><input type="checkbox" name="GHS_pictogram_codes[]" id="GHS09" value="GHS09"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <hr />
                                    <div class="form-group">
                                        <label for="GHS_H_codes"><b>GHS Hazard (H) Statements</b></label><br />
                                        <small>Enter only the H Statement codes (i.e. H301). Do not add additional spaces. Separate H Statement codes with a comma.</small><br />
                                        <small>If no H Statements apply, just skip this section.</small>
                                        <input type="text" name="GHS_H_codes" id="GHS_H_codes" class="form-control" placeholder="example: H220,H310,H301+H311+H331" style="margin-top: 5px;">
                                    </div>
                                    <hr />
                                    <div class="form-group">
                                        <label for="GHS_P_codes"><b>GHS Precautionary (P) Statements</b></label><br />
                                        <small>Enter only the P Statement codes (i.e. P310). Do not add additional spaces. Separate P Statement codes with a comma.</small><br />
                                        <small>If no P Statements apply, just skip this section.</small>
                                        <input type="text" name="GHS_P_codes" id="GHS_P_codes" class="form-control" placeholder="example: P273,P360,P371+P380+P375" style="margin-top: 5px;">
                                    </div>
                                    <hr />
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-2 pt-0"><b>Location:</b></legend>
                                            <div class="col-sm-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="CC" value="CC" checked>
                                                    <label class="form-check-label" for="CC">Chemical Cabinet</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="fridge1" value="Fridge 1">
                                                    <label class="form-check-label" for="fridge1">Fridge 1</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="fridge2" value="Fridge 2">
                                                    <label class="form-check-label" for="fridge2">Fridge 2</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="freezer1" value="Freezer 1">
                                                    <label class="form-check-label" for="freezer1">Freezer 1</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="freezer2" value="Freezer 2">
                                                    <label class="form-check-label" for="freezer2">Freezer 2</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="H1C" value="H1C">
                                                    <label class="form-check-label" for="H1C">Fume Hood 1 Cabinet</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="H2C" value="H2C">
                                                    <label class="form-check-label" for="H2C">Fume Hood 2 Cabinet</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="CR" value="CR">
                                                    <label class="form-check-label" for="CR">Chemical Room</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="PC" value="PC">
                                                    <label class="form-check-label" for="PC">Poison Cabinet</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="location" id="other" value="other">
                                                    <label class="form-check-label" for="other">Other <input type="text" name="other_location" class="form-control" placeholder="Custom location" id="other_location"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr />
                                    <div class="form-group">
                                        <legend class="form-label"><b>First-Aid Statements</b></legend>
                                        <small>Consult the chemical's Safety Data Sheet (Section 4) for these statements.</small><br />
                                        <legend class="form-label">English Statements</legend>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_inhalation_EN">After Inhalation: </label>
                                            </div>
                                            <select class="custom-select" id="after_inhalation_EN" name="after_inhalation_EN">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_inhalation_EN">If breathed in, move person into fresh air. If not breathing, give artificial respiration. Consult a physician.</option>
                                                <option value="severe_inhalation_EN">Call a physician immediately. If breathing is irregular or stopped, administer artificial respiration.</option>
                                                <option value="other_inhalation_EN">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_inhalation_EN_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_inhalation_EN_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_skin_contact_EN">After Skin Contact: </label>
                                            </div>
                                            <select class="custom-select" id="after_skin_EN" name="after_skin_EN">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_skin_EN">Wash off with soap and plenty of water. Consult a physician.</option>
                                                <option value="severe_skin_EN">After contact with skin, wash immediately with plenty of water. In case of extensive skin contact serious poisoning possible. Call a physician in any case.</option>
                                                <option value="other_skin_EN">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_skin_EN_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_skin_EN_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_eye_contact_EN">After Eye Contact: </label>
                                            </div>
                                            <select class="custom-select" id="after_eye_EN" name="after_eye_EN">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_eye_EN">Flush eyes with water as a precaution.</option>
                                                <option value="severe_eye_EN">Rinse cautiously with water for several minutes. In all cases of doubt, or when symptoms persist, seek medical advice.</option>
                                                <option value="other_eye_EN">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_eye_EN_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_eye_EN_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_ingestion_EN">After Ingestion: </label>
                                            </div>
                                            <select class="custom-select" id="after_ingestion_EN" name="after_ingestion_EN">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_ingestion_EN">Never give anything by mouth to an unconscious person. Rinse mouth with water. Consult a physician.</option>
                                                <option value="severe_ingestion_EN">Rinse mouth immediately and drink plenty of water. Call a physician immediately.</option>
                                                <option value="other_ingestion_EN">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_ingestion_EN_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_ingestion_EN_text"></textarea>
                                        <legend class="form-label">German Statements</legend>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_inhalation_DE">Nach Einatmen: </label>
                                            </div>
                                            <select class="custom-select" id="after_inhalation_DE" name="after_inhalation_DE">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_inhalation_DE">Bei Einatmen, betroffene Person an die frische Luft bringen. Bei Atemstillstand, künstlich beatmen. Arzt konsultieren.</option>
                                                <option value="severe_inhalation_DE">Sofort Arzt hinzuziehen. Bei Atembeschwerden oder Atemstillstand künstliche Beatmung einleiten.</option>
                                                <option value="other_inhalation_DE">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_inhalation_DE_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_inhalation_DE_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_skin_contact_DE">Nach Hautkontakt: </label>
                                            </div>
                                            <select class="custom-select" id="after_skin_DE" name="after_skin_DE">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_skin_DE">Mit Seife und viel Wasser abwaschen. Arzt konsultieren.</option>
                                                <option value="severe_skin_DE">Bei Berührung mit der Haut sofort abwaschen mit viel Wasser. Bei großflächigem Hautkontakt schwere Vergiftung möglich. Unbedingt Arzt hinzuziehen.</option>
                                                <option value="other_skin_DE">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_skin_DE_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_skin_DE_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_eye_contact_DE">Nach Augenkontakt: </label>
                                            </div>
                                            <select class="custom-select" id="after_eye_DE" name="after_eye_DE">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_eye_DE">Augen vorsorglich mit Wasser ausspülen.</option>
                                                <option value="severe_eye_DE">Einige Minuten lang behutsam mit Wasser ausspülen. Bei Auftreten von Beschwerden oder in Zwei felsfällen ärztlichen Rat einholen.</option>
                                                <option value="other_eye_DE">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_eye_DE_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_eye_DE_text"></textarea>
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="after_ingestion_DE">Nach Verschlucken: </label>
                                            </div>
                                            <select class="custom-select" id="after_ingestion_DE" name="after_ingestion_DE">
                                                <option value="" selected>Choose...</option>
                                                <option value="default_ingestion_DE">Nie einer ohnmächtigen Person etwas durch den Mund einflößen. Mund mit Wasser ausspülen. Arzt konsultieren.</option>
                                                <option value="severe_ingestion_DE">Sofort Mund ausspülen und reichlich Wasser nachtrinken. Sofort Arzt hinzuziehen.</option>
                                                <option value="other_ingestion_DE">Other</option>
                                            </select>
                                        </div>
                                        <textarea name="other_ingestion_DE_text" class="form-control input-custom-margin safety-statements-other-input" placeholder="Other..." id="other_ingestion_DE_text"></textarea>
                                    </div>
                                    <hr />
                                    <div class="form-group">
                                        <legend class="form-label"><b>Safety Data Sheets</b></legend>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="english_sds_addon">English SDS</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="english_sds" aria-describedby="english_sds_addon" name="english_sds">
                                                <label class="custom-file-label" for="english_sds">Choose file (only PDF!)</label>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="german_sds_addon">German SDS</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="german_sds" aria-describedby="german_sds_addon" name="german_sds">
                                                <label class="custom-file-label" for="german_sds">Choose file (only PDF!)</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger close_modal">Cancel</button>
                <a href="#" class="btn btn-success" id="add_chemical">Add Chemical</a>
            </div>
        </div>
    </div>
</div>
