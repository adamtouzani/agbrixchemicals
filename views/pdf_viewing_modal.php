<div class="modal fade" tabindex="-1" role="dialog" id="pdf_modal">
    <div class="modal-dialog modal-lg" role="document" style="min-height: 70%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="chemical_name_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 100%">
                <div style="height: 100%">
                    <iframe src="" type="application/pdf" style="width:100%; height:100%" id="pdf_embed_element">Your browser doesn't support the display of PDF files. Try to download the PDF instead.</iframe>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary" id="download_as_pdf">Download as PDF</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
