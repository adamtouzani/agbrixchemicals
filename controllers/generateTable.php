<?php

function generateTables($mysqli_rows, $showLocation = false) {
    echo '<table class="table table-bordered" style="table-layout: fixed;">
            <thead>
              <tr>
                <th class="text-center align-top" style="width: 20%;">Chemical<br /><small>(Company, Article Number)</small></th>
                <th class="text-center align-top" style="width: 10%">CAS Number</th>
                <th class="text-center align-top" style="width: 10%">GHS Pictograms</th>
                <th class="text-center align-top" style="width: 20%">GHS H- and P- phrases</th>
                <th class="text-center align-top" style="width: 8%;">Quantity</th>
                <th class="text-center align-top" style="width: 11%">SDS</th>
                <th class="text-center align-top" style="width: 11%">SOP</th>';
    if($showLocation) {
        echo '<th class="text-center align-top" style="width: 10%">Location</th>';
    }
    echo '</tr></thead><tbody>';

    if(mysqli_num_rows($mysqli_rows) > 0){
        while($row = mysqli_fetch_assoc($mysqli_rows)){
            $CAS_Numbers = explode(",", str_replace(" ", "", $row['CAS_number']));
            $GHS_pictogram_codes = explode("\\", str_replace(" ", "", $row["GHS_pictogram_codes"]));
            $GHS_H_codes = explode("\\", str_replace(" ", "", $row["GHS_H_codes"]));
            $GHS_P_codes = explode("\\", str_replace(" ", "", $row["GHS_P_codes"]));
            echo '<tr>';
            echo '<td class="text-center">'.$row["chemical_name"].'<br /><small>('.$row["company"].', '.$row["article_number"].')</small><div id="chemical_'.$row['id'].'" style="display: none;">'.$row['chemical_name'].'-'.$row['CAS_number'].'</div></td>';
            echo '<td class="text-center">';
            $i = 0;
            foreach ($CAS_Numbers as $key => $value) {
                if(!$value){
                    echo "No CAS Number";
                } else if (++$i == count($CAS_Numbers)) {
                    echo $value;
                } else {
                    echo $value.",<br>";
                }
            }
            echo '</td>';
            echo '<td class="text-center">';
            foreach ($GHS_pictogram_codes as $key => $value) {
                if(!$value){
                    echo "No GHS Pictogram";
                } else {
                    echo '<img class="img-responsive" width="50" src="GHS_symbols/'.$value.'.svg"></img>';
                }
            }
            echo '</td>';
            echo '<td class="text-center">';
            $i = 0;
            echo "<b>H:</b>";
            foreach ($GHS_H_codes as $key => $value) {
                if(!$value){
                    echo "No GHS H phrases/No SDS";
                } else if (++$i == count($GHS_H_codes)) {
                    echo $value;
                } else {
                    echo $value.", ";
                }
            }
            $i = 0;
            echo "<br><b>P:</b>";
            foreach ($GHS_P_codes as $key => $value) {
                if(!$value){
                    echo "No GHS P phrases/No SDS";
                } else if (++$i == count($GHS_P_codes)) {
                    echo $value;
                } else {
                    echo $value.", ";
                }
            }
            echo '</td>';
            $i = 0;

            echo '<td class="text-center">'.$row["weight_quantity"].'</td>';
            // MSDS
            echo '<td class="text-center">';
            if ($row['MSDS_EN']=="Yes") {
                echo '<a href="#" id="sds_en_'.$row['id'].'">English</a><br/>';
            } else {
                echo 'No English SDS<br/>';
            }
            if ($row['MSDS_DE']=="Yes") {
                echo '<a href="#" id="sds_de_'.$row['id'].'">German</a>';
            } else {
                echo 'No German SDS';
            }
            echo '</td>';
            // SOP
            echo '<td class="text-center">';
            if ($row['SOP_EN']=="Yes") {
                echo '<a href="#" id="sop_en_'.$row['id'].'">English</a><br/>';
            } else {
                echo 'No English SOP<br/>';
            }
            if ($row['SOP_DE']=="Yes") {
                echo '<a href="#" id="sop_de_'.$row['id'].'">German</a>';
            } else {
                echo 'No German SOP';
            }
            echo '</td>';
            if($showLocation) {
                echo '<td class="text-center">'.$row["location"].'</td>';
            }
            echo '<tr>';
        }
    }
    echo '</tbody>
          </table>';

}