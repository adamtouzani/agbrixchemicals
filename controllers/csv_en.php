<?php

require('checkSession.php');

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=indesign_EN.csv');


require '../../connectvars.php';

$db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
if(!$db_connection){
    echo "error connecting to DB!";
    die(mysqli_connect_error());
}
mysqli_set_charset ($db_connection, "utf8");

$GHS_H_codes_values_EN = array();
$GHS_H_codes_values_DE = array();

$query = "SELECT * FROM H_codes";
$result = mysqli_query($db_connection, $query);
while($row = mysqli_fetch_assoc($result)){
    $GHS_H_codes_values_EN[str_replace(" ", "", $row["code"])] = $row["statement_EN"];
    $GHS_H_codes_values_DE[str_replace(" ", "", $row["code"])] = $row["statement_DE"];
}

$GHS_P_codes_values = array();

$query = "SELECT * FROM P_codes";
$result = mysqli_query($db_connection, $query);
while($row = mysqli_fetch_assoc($result)){
    $GHS_P_codes_values_EN[str_replace(" ", "", $row["code"])] = $row["statement_EN"];
    $GHS_P_codes_values_DE[str_replace(" ", "", $row["code"])] = $row["statement_DE"];
}

$file = fopen('php://output', "w");

fwrite($file, "chemical_name_EN,article_number,@pictogram1,@pictogram2,@pictogram3,@pictogram4,@pictogram5,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10\n");

$query = "SELECT * FROM all_chemicals WHERE location = 'PC' OR GHS_H_codes IS NOT NULL OR GHS_P_codes IS NOT NULL OR GHS_Pictogram_codes IS NOT NULL";
$chemicals = mysqli_query($db_connection, $query);

while($row=mysqli_fetch_assoc($chemicals)){
    fwrite($file, "\"".$row["chemical_name"]."\",");
    fwrite($file, "\"".$row["article_number"]."\",");
    $GHS_pictogram_codes = explode("\\", str_replace(" ", "", $row["GHS_pictogram_codes"]));
    $GHS_H_codes = explode("\\", str_replace(" ", "", $row["GHS_H_codes"]));
    $GHS_P_codes = explode("\\", str_replace(" ", "", $row["GHS_P_codes"]));
    for ($i=0; $i < 5; $i++) {
        if($i < count($GHS_pictogram_codes)){
            if($GHS_pictogram_codes[$i]==""){
                fwrite($file, "no_image.png,");
            } else {
                fwrite($file, $GHS_pictogram_codes[$i].".png,");
            }

        } else {
            fwrite($file, "no_image.png,");
        }
    }
    for ($i=0; $i < 10; $i++) {
        if($GHS_H_codes[0]=="" && $i == 0){
            fwrite($file, "No H Statements/No Safety Data Sheet available,");
        } else if($i < count($GHS_H_codes)){
            if($GHS_H_codes[$i]!=""){
                if($GHS_H_codes_values_EN[$GHS_H_codes[$i]]==""){
                    $individual_codes = explode("+", $GHS_H_codes[$i]);
                    $text = "";
                    foreach ($individual_codes as $value) {
                        $text .= $GHS_H_codes_values_EN[$value]." ";
                    }
                    fwrite($file, "\"".$GHS_H_codes[$i].": ".$text."\",");
                } else {
                    fwrite($file, "\"".$GHS_H_codes[$i].": ".$GHS_H_codes_values_EN[$GHS_H_codes[$i]]."\",");
                }
            } else {
                fwrite($file, ",");
            }
        } else {
            fwrite($file, ",");
        }
    }
    for ($i=0; $i < 10; $i++) {
        if($GHS_P_codes[0]=="" && $i == 0){
            fwrite($file, "No P Statements/No Safety Data Sheet available,");
        } else if($i < count($GHS_P_codes)){
            if($GHS_P_codes[$i]!=""){
                if($GHS_P_codes_values_EN[$GHS_P_codes[$i]]==""){
                    $individual_codes = explode("+", $GHS_P_codes[$i]);
                    $text = "";
                    foreach ($individual_codes as $value) {
                        $text .= $GHS_P_codes_values_EN[$value]." ";
                    }
                    fwrite($file, "\"".$GHS_P_codes[$i].": ".$text."\",");
                } else {
                    fwrite($file, "\"".$GHS_P_codes[$i].": ".$GHS_P_codes_values_EN[$GHS_P_codes[$i]]."\",");
                }
            } else {
                fwrite($file, ",");
            }
        } else if ($i == 9) {
            fwrite($file, "\n");
        } else {
            fwrite($file, ",");
        }
    }
}

fclose($file);
