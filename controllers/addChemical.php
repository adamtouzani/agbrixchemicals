<?php

require('checkSession.php');

if($_SERVER['REQUEST_METHOD'] === "POST"){
    // Validate H & P phrases to ensure only allowed characters are present
    $matchPatternHPhrases = "/^(?:(?:EU)?H\d{3}(?:A|i|f|F|d|D){0,2}(?:(?:,|\+)(?:EU)?H\d{3}(?:A|i|f|F|d|D){0,2})*)?$/";
    $matchPatternPPhrases = "/^(?:P\d{3}(?:(?:,|\+)P\d{3})*)?$/";


    $response = (object)[];

    $response->post = $_FILES;

    foreach($_FILES as $file) {
        $detected_type = mime_content_type($file['tmp_name']);
        if($detected_type != "application/pdf") {
            header('HTTP/1.1 400 Bad Request');
            $response->error = "You can only upload PDF files with this form.";
            $response->message = "Make sure that both SDS files are in PDF format. If not, convert the file to PDF before uploading the file here.";
            die(json_encode($response));
        }
    }

    if(!preg_match($matchPatternHPhrases, $_POST['GHS_H_codes']) || !preg_match($matchPatternPPhrases, $_POST['GHS_P_codes'])){
        header('HTTP/1.1 400 Bad Request');
        $response->error = "You have entered the GHS H or P codes incorrectly.";
        $response->message = "The GHS H and P codes cannot have any spaces, or any characters not allowed in the GHS scheme. Only separate GHS H or P codes with a comma.";
        die(json_encode($response));
    }

    if(!$_POST['official_name_EN'] || !$_POST['official_name_DE']) {
        header('HTTP/1.1 400 Bad Request');
        $response->error = "You did not provide the chemical name";
        $response->message = "Both the English and German chemical names are required to add the chemical to the DB.";
        die(json_encode($response));
    }

    if(!$_POST['company'] || !$_POST['article_number']) {
        header('HTTP/1.1 400 Bad Request');
        $response->error = "You did not provide the company and/or article number";
        $response->message = "Both the company name and article number are required to add the chemical to the DB. If these are unknown".
            " or do not apply, simply put '?' or 'N/A' in the respective fields.";
        die(json_encode($response));
    }

    if(!$_POST['weight_quantity']) {
        header('HTTP/1.1 400 Bad Request');
        $response->error = "You did not provide the quantity & weight";
        $response->message = "The weight and/or quantity is/are required to add a new chemical to the DB.";
        die(json_encode($response));
    }

    $GHS_H_codes = str_replace(',', '\\\\', $_POST['GHS_H_codes']);
    $GHS_P_codes = str_replace(',', '\\\\', $_POST['GHS_P_codes']);
    $GHS_pictogram_codes = array_key_exists('GHS_pictogram_codes', $_POST) ? join("\\\\", $_POST['GHS_pictogram_codes']) : null;

    $messages = array(
        "default_inhalation_EN" => "If breathed in, move person into fresh air. If not breathing, give artificial respiration. Consult a physician.",
        "severe_inhalation_EN" => "Call a physician immediately. If breathing is irregular or stopped, administer artificial respiration.",
        "other_inhalation_EN" => $_POST['other_inhalation_EN_text'],
        "default_skin_EN" => "Wash off with soap and plenty of water. Consult a physician.",
        "severe_skin_EN" => "After contact with skin, wash immediately with plenty of water. In case of extensive skin contact serious poisoning possible. Call a physician in any case.",
        "other_skin_EN" => $_POST['other_skin_EN_text'],
        "default_eye_EN" => "Flush eyes with water as a precaution.",
        "severe_eye_EN" => "Rinse cautiously with water for several minutes. In all cases of doubt, or when symptoms persist, seek medical advice.",
        "other_eye_EN" => $_POST['other_eye_EN_text'],
        "default_ingestion_EN" => "Never give anything by mouth to an unconscious person. Rinse mouth with water. Consult a physician.",
        "severe_ingestion_EN" => "Rinse mouth immediately and drink plenty of water. Call a physician immediately.",
        "other_ingestion_EN" => $_POST['other_ingestion_EN_text'],
        "default_inhalation_DE" => "Bei Einatmen, betroffene Person an die frische Luft bringen. Bei Atemstillstand, künstlich beatmen. Arzt konsultieren.",
        "severe_inhalation_DE" => "Sofort Arzt hinzuziehen. Bei Atembeschwerden oder Atemstillstand künstliche Beatmung einleiten.",
        "other_inhalation_DE" => $_POST['other_inhalation_DE_text'],
        "default_skin_DE" => "Mit Seife und viel Wasser abwaschen. Arzt konsultieren.",
        "severe_skin_DE" => "Bei Berührung mit der Haut sofort abwaschen mit viel Wasser. Bei großflächigem Hautkontakt schwere Vergiftung möglich. Unbedingt Arzt hinzuziehen.",
        "other_skin_DE" => $_POST['other_skin_DE_text'],
        "default_eye_DE" => "Augen vorsorglich mit Wasser ausspülen.",
        "severe_eye_DE" => "Einige Minuten lang behutsam mit Wasser ausspülen. Bei Auftreten von Beschwerden oder in Zwei felsfällen ärztlichen Rat einholen.",
        "other_eye_DE" => $_POST['other_eye_DE_text'],
        "default_ingestion_DE" => "Nie einer ohnmächtigen Person etwas durch den Mund einflößen. Mund mit Wasser ausspülen. Arzt konsultieren.",
        "severe_ingestion_DE" => "Sofort Mund ausspülen und reichlich Wasser nachtrinken. Sofort Arzt hinzuziehen.",
        "other_ingestion_DE" => $_POST['other_ingestion_DE_text'],
        "" => null
    );

    if($GHS_pictogram_codes) {
        if(!$messages[$_POST['after_inhalation_EN']] ||
            !$messages[$_POST['after_skin_EN']] ||
            !$messages[$_POST['after_eye_EN']] ||
            !$messages[$_POST['after_ingestion_EN']] ||
            !$messages[$_POST['after_inhalation_DE']] ||
            !$messages[$_POST['after_skin_DE']] ||
            !$messages[$_POST['after_eye_DE']] ||
            !$messages[$_POST['after_ingestion_DE']]
        ) {
            header('HTTP/1.1 400 Bad Request');
            $response->error = "You did not provide all the first-aid statements";
            $response->message = "Since you selected at least one GHS Pictogram for this chemical, all the first-aid statements (in both English and German) must be set.";
            ob_clean();
            die(json_encode($response));
        }
    }
    
    $query = "INSERT INTO 
                `all_chemicals` 
                    (chemical_name,
                     official_name_EN,
                     official_name_DE,
                     company,
                     article_number,
                     CAS_number,
                     weight_quantity,
                     GHS_pictogram_codes,
                     GHS_H_codes,
                     GHS_P_codes,
                     location,
                     MSDS_EN,
                     MSDS_DE,
                     SOP_EN,
                     SOP_DE,
                     after_inhalation_EN,
                     after_skin_EN,
                     after_eye_EN,
                     after_ingestion_EN,
                     after_inhalation_DE,
                     after_skin_DE,
                     after_eye_DE,
                     after_ingestion_DE)
                VALUES 
                   ('{$_POST['official_name_EN']}', 
                    '{$_POST['official_name_EN']}',
                    '{$_POST['official_name_DE']}',
                    '{$_POST['company']}', 
                    '{$_POST['article_number']}', 
                    '{$_POST['CAS_number']}', 
                    '{$_POST['weight_quantity']}',
                    '{$GHS_pictogram_codes}',
                    '{$GHS_H_codes}',
                    '{$GHS_P_codes}',
                    '{$_POST['location']}',
                    'Yes', 'Yes', 'No', 'No',
                    '{$messages[$_POST['after_inhalation_EN']]}',
                    '{$messages[$_POST['after_skin_EN']]}',
                    '{$messages[$_POST['after_eye_EN']]}',
                    '{$messages[$_POST['after_ingestion_EN']]}',
                    '{$messages[$_POST['after_inhalation_DE']]}',
                    '{$messages[$_POST['after_skin_DE']]}',
                    '{$messages[$_POST['after_eye_DE']]}',
                    '{$messages[$_POST['after_ingestion_DE']]}')";

    require '../../connectvars.php';
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
    mysqli_set_charset ($db_connection, "utf8");

    if(!$db_connection){
        ob_clean();
        header('HTTP/1.1 500 Internal Server Error');
        $response->error = "Internal Server Error";
        $response->message = mysqli_connect_error();
        die(json_encode($response));
    }

    mysqli_query($db_connection, $query);

    $id = mysqli_insert_id($db_connection);

    if($id == 0) {
        ob_clean();
        header('HTTP/1.1 500 Internal Server Error');
        $response->error = "Internal Server Error";
        $response->message = mysqli_connect_error();
        mysqli_close($db_connection);
        die(json_encode($response));
    }

    $response->id = $id;
    $target_dir = '../SDS/';
    $SDS_EN = $target_dir.'EN/'.$id.'.pdf';
    $SDS_DE = $target_dir.'DE/'.$id.'.pdf';

    if (!move_uploaded_file($_FILES['english_sds']['tmp_name'], $SDS_EN) ||
        !move_uploaded_file($_FILES['german_sds']['tmp_name'], $SDS_DE)) {
        $query = "DELETE FROM `all_chemicals` WHERE `id` = '{$id}'";
        mysqli_query($db_connection, $query);
        ob_clean();
        header('HTTP/1.1 500 Internal Server Error');
        $response->error = "Internal Server Error";
        $response->message = "Unable to copy uploaded files. Try again at a later time.";
        mysqli_close($db_connection);
        die(json_encode($response));
    }


    $response->message = "Success!";
    $response->details = "The chemical ".$_POST['official_name_EN']." was successfully added to the database";

    mysqli_close($db_connection);
    ob_clean();
    exit(json_encode($response));

}


