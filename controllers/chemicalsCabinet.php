<?php

require('checkSession.php');

if($_SERVER['REQUEST_METHOD'] === "GET"){
    require '../../connectvars.php';

    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
    if(!$db_connection){
        echo "error connecting to DB!";
        die(mysqli_connect_error());
    }
    mysqli_set_charset ($db_connection, "utf8");

    $query = "SELECT id, chemical_name, company, article_number, CAS_number, weight_quantity, GHS_pictogram_codes, GHS_H_codes, GHS_P_codes, MSDS_EN, MSDS_DE, SOP_EN, SOP_DE FROM all_chemicals WHERE location = 'CC' ORDER BY chemical_name";
    $result = mysqli_query($db_connection, $query);

    echo '<br />
			<h3 style="text-align: center;">Chemicals in Chemical Cabinet</h3>
			<br />';

    require('generateTable.php');
    generateTables($result);

    mysqli_close($db_connection);
}
