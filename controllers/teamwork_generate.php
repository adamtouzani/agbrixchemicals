<ac:layout>
    <?php
    require '../../connectvars.php';

    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
    if(!$db_connection){
        echo "error connecting to DB!";
        die(mysqli_connect_error());
    }
    mysqli_set_charset ($db_connection, "utf8");

    //GHS Pictogram Code and Value
    $GHS_pictogram_values_EN = array(
        'GHS01' => 'Explosive',
        'GHS02' => 'Flammable',
        'GHS03' => 'Oxidizing',
        'GHS04' => 'Compressed Gas',
        'GHS05' => 'Corrosive',
        'GHS06' => 'Toxic',
        'GHS07' => 'Harmful',
        'GHS08' => 'Health Hazard',
        'GHS09' => 'Environmental Hazard'
    );
    $GHS_pictogram_values_DE = array(
        'GHS01' => 'Explodierende Bombe',
        'GHS02' => 'Flamme',
        'GHS03' => 'Flamme über Kreis',
        'GHS04' => 'Gasflasche',
        'GHS05' => 'Ätzwirkung',
        'GHS06' => 'Totenkopf mit gekreuzten Knochen',
        'GHS07' => 'Ausrufezeichen',
        'GHS08' => 'Gesund­heits­gefahr',
        'GHS09' => 'Umwelt'
    );
    ?>
    <ac:layout-section ac:type="single">
        <ac:layout-cell>
            <table>
                <tbody>
                <tr>
                    <th style="text-align: center;">Chemical Name</th>
                    <th style="text-align: center;">Company</th>
                    <th style="text-align: center;">Article No.</th>
                    <th style="text-align: center;">CAS-Number</th>
                    <th colspan="1" style="text-align: center;">GHS Pictogram</th>
                    <th style="text-align: center;">
                        <span style="color: rgb(0,0,0);">Classification (Hazardous Properties)</span>
                    </th>
                    <th colspan="1" style="text-align: center;">H- &amp; P-Statements</th>
                    <th style="text-align: center;">SDS</th>
                    <th style="text-align: center;">SOP</th>
                </tr>
                <?php
                $query = "SELECT * FROM all_chemicals ORDER BY chemical_name ASC";
                $result = mysqli_query($db_connection, $query);
                while ($row = mysqli_fetch_assoc($result)) {
                    //Get individual codes for pictograms, H & P statements, CAS Numbers
                    $CAS_Numbers = explode(",", str_replace(" ", "", $row['CAS_number']));
                    $GHS_pictogram_codes = explode("\\", str_replace(" ", "", $row["GHS_pictogram_codes"]));
                    $GHS_H_codes = explode("\\", str_replace(" ", "", $row["GHS_H_codes"]));
                    $GHS_P_codes = explode("\\", str_replace(" ", "", $row["GHS_P_codes"]));
                    echo '<tr>';
                    echo '<td style="text-align: center;">'.$row["chemical_name"].'</td>';
                    if($row["company"]=="?"){
                        echo '<td style="text-align: center;">Unknown (Aliquoted)</td><td>Unknown (Aliquoted)</td>';
                    } else {
                        echo '<td style="text-align: center;">'.$row["company"].'</td><td>'.str_replace("?", "Unknown", $row["article_number"]).'</td>';
                    }
                    echo '<td style="text-align: center;">';
                    $i = 0;
                    foreach ($CAS_Numbers as $key => $value) {
                        if(!$value || $value == '?'){
                            echo "No CAS Number";
                        } else if (++$i == count($CAS_Numbers)) {
                            echo $value;
                        } else {
                            echo $value.",<br/>";
                        }
                    }
                    echo '</td>';
                    echo '<td colspan="1" style="text-align: center;">';
                    foreach ($GHS_pictogram_codes as $pic) {
                        if(!$pic){
                            echo 'No GHS Pictogram(s)';
                        } else {
                            echo '<ac:image ac:thumbnail="true" ac:width="60">';
                            echo '<ri:attachment ri:filename="'.$pic.'.png"/>';
                            echo '</ac:image>';
                        }

                    }
                    echo '</td>';
                    echo '<td style="text-align: center;">';
                    foreach ($GHS_pictogram_codes as $code) {
                        if(!$code){
                            echo 'Not classified as a hazardous substance';
                        } else {
                            echo '<b>'.$GHS_pictogram_values_EN[$code].'</b><br/><small>'.$GHS_pictogram_values_DE[$code].'</small><br/>';
                        }
                    }
                    echo '</td>';
                    $i = 0;
                    echo '<td style="text-align: center;">';
                    echo "<b>H:</b>";
                    foreach ($GHS_H_codes as $key => $value) {
                        if(!$value){
                            echo "No GHS H phrases/No SDS";
                        } else if (++$i == count($GHS_H_codes)) {
                            echo $value;
                        } else {
                            echo $value.", ";
                        }
                    }
                    $i = 0;
                    echo "<br/><b>P:</b>";
                    foreach ($GHS_P_codes as $key => $value) {
                        if(!$value){
                            echo "No GHS P phrases/No SDS";
                        } else if (++$i == count($GHS_P_codes)) {
                            echo $value;
                        } else {
                            echo $value.", ";
                        }
                    }
                    echo '</td>';
                    echo '<td style="text-align: center;">';
                    if($row["MSDS"] == "No" || $row["MSDS"] == "No - Kit"){
                        echo 'No SDS is available for this chemical.';
                    } else {
                        echo '<ac:link>';
                        echo '<ri:attachment ri:filename="'.$row["article_number"].'_EN.pdf"/>';
                        echo '<ac:link-body>English.pdf</ac:link-body>';
                        echo '</ac:link>';
                        echo '<br/>';
                        if($row["MSDS"] != "Yes (EN Only)"){
                            echo '<ac:link>';
                            echo '<ri:attachment ri:filename="'.$row["article_number"].'_DE.pdf"/>';
                            echo '<ac:link-body>Deutsche.pdf</ac:link-body>';
                            echo '</ac:link>';
                        }
                        echo '</td>';
                    }

                    echo '<td style="text-align: center;">';
                    if(!$row["GHS_pictogram_codes"]){
                        echo 'No SOP is available for this chemical.';
                    } else {
                        echo '<ac:link>';
                        echo '<ri:attachment ri:filename="'.$row["article_number"].'_SOP_EN.pdf"/>';
                        // echo '<ac:plain-text-link-body><![CDATA[SOP_English.pdf]]></ac:plain-text-link-body>';
                        echo '</ac:link>';
                        echo '<br/>';
                        echo '<ac:link>';
                        echo '<ri:attachment ri:filename="'.$row["article_number"].'_SOP_DE.pdf"/>';
                        // echo '<ac:plain-text-link-body><![CDATA[SOP_Deutsche.pdf]]></ac:plain-text-link-body>';
                        echo '</ac:link>';
                    }
                    echo '</td>';

                    echo '</tr>';
                }

                ?>
                </tbody>
            </table>
        </ac:layout-cell>
    </ac:layout-section>
</ac:layout>
