<?php

require('checkSession.php');

//Determine privileges
if($_SESSION['userType']!="admin"){
    header('Location: ../chemicalDB.php');
    die();
}

?>


<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="AG Brix Chemicals Database">
    <meta name="author" content="Adam Touzani">
    <title>AG Brix Chemicals - Gefahrstoffkataster</title>
</head>
<body>
<?php

require '../../connectvars.php';

$db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
if(!$db_connection){
    echo "error connecting to DB!";
    die(mysqli_connect_error());
}
mysqli_set_charset ($db_connection, "utf8");

// <link href="../css/gefahrstoffkataster.css" rel="stylesheet">
echo '<link href="../css/gefahrstoffkataster.css" rel="stylesheet">
  <br />
			<h3 style="text-align: center;">Gefahrstoffkataster</h3>
			<br />
			<table class="table table-bordered" style="table-layout: fixed;">
				<thead>
					<tr>
						<th class="text-center align-top" style="width: 15%;">Chemical Name (English)</th>
            <th class="text-center align-top" style="width: 15%;">Chemical Name (German)</th>
						<th class="text-center align-top" style="width: 10%">Company,<br>Article Number</th>
						<th class="text-center align-top" style="width: 10%">CAS Number</th>
						<th class="text-center align-top" style="width: 10%">GHS Pictograms</th>
						<th class="text-center align-top" style="width: 24%">GHS H- and P- phrases</th>
						<th class="text-center align-top" style="width: 7%;">Quantity</th>
            <th class="text-center align-top" style="width: 9%">Location</th>
					</tr>
				</thead>
				<tbody>';
$query = "SELECT id, chemical_name, official_name_DE, company, article_number, CAS_number, weight_quantity, GHS_pictogram_codes, GHS_H_codes, GHS_P_codes, MSDS_EN, MSDS_DE, SOP_EN, SOP_DE, location FROM all_chemicals WHERE GHS_pictogram_codes IS NOT NULL ORDER BY chemical_name";
$result = mysqli_query($db_connection, $query);
if(mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        $CAS_Numbers = explode(",", str_replace(" ", "", $row['CAS_number']));
        $GHS_pictogram_codes = explode("\\", str_replace(" ", "", $row["GHS_pictogram_codes"]));
        $GHS_H_codes = explode("\\", str_replace(" ", "", $row["GHS_H_codes"]));
        $GHS_P_codes = explode("\\", str_replace(" ", "", $row["GHS_P_codes"]));
        echo '<tr>';
        echo '<td class="text-center">'.$row["chemical_name"].'<div id="chemical_'.$row['id'].'" style="display: none;">'.$row['chemical_name'].'-'.$row['CAS_number'].'</div></td>';
        echo '<td class="text-center">'.$row["official_name_DE"].'</td>';
        echo '<td class="text-center">'.$row["company"].',<br>'.$row["article_number"].'</td>';
        echo '<td class="text-center">';
        $i = 0;
        foreach ($CAS_Numbers as $key => $value) {
            if(!$value){
                echo "No CAS Number";
            } else if (++$i == count($CAS_Numbers)) {
                echo $value;
            } else {
                echo $value.",<br>";
            }
        }
        echo '</td>';
        echo '<td class="text-center">';
        foreach ($GHS_pictogram_codes as $key => $value) {
            if(!$value){
                echo "No GHS Pictogram";
            } else {
                echo '<img class="img-responsive" width="50" src="../GHS_symbols/'.$value.'.svg"></img>';
            }
        }
        echo '</td>';
        echo '<td class="text-center">';
        $i = 0;
        echo "<b>H:</b>";
        foreach ($GHS_H_codes as $key => $value) {
            if(!$value){
                echo "No GHS H phrases/No SDS";
            } else if (++$i == count($GHS_H_codes)) {
                echo $value;
            } else {
                echo $value.", ";
            }
        }
        $i = 0;
        echo "<br><b>P:</b>";
        foreach ($GHS_P_codes as $key => $value) {
            if(!$value){
                echo "No GHS P phrases/No SDS";
            } else if (++$i == count($GHS_P_codes)) {
                echo $value;
            } else {
                echo $value.", ";
            }
        }
        echo '</td>';
        $i = 0;

        echo '<td class="text-center">'.$row["weight_quantity"].'</td>';

        echo '</td>';
        echo '<td class="text-center">'.$row["location"].'</td>';
        echo '<tr>';
    }
}
echo '</tbody>
			</table>';
?>
</body>
