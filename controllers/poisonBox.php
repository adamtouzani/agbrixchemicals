<?php

require('checkSession.php');

if($_SERVER['REQUEST_METHOD'] === "GET"){
    require '../../connectvars.php';

    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
    if(!$db_connection){
        echo "error connecting to DB!";
        die(mysqli_connect_error());
    }
    mysqli_set_charset ($db_connection, "utf8");


    $query = "SELECT id, chemical_name, company, article_number, CAS_number, weight_quantity, GHS_pictogram_codes, GHS_H_codes, GHS_P_codes, MSDS_EN, MSDS_DE, SOP_EN, SOP_DE FROM all_chemicals WHERE location = 'PC' ORDER BY chemical_name";
    $result = mysqli_query($db_connection, $query);

    echo '<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-2" style="text-align: right;">
					<img src="GHS_symbols/GHS06.svg" width="100" height="100">
				</div>
				<div class="col-md-4" style="margin-top: 18px">
					<h3 style="text-align: center">Toxic Chemicals</h3>
					<h3 style="text-align: center">Giftige Chemikalien</h3>
				</div>
				<div class="col-md-3"></div>
			</div>
			<br />';

    require('generateTable.php');
    generateTables($result);

    mysqli_close($db_connection);
}
