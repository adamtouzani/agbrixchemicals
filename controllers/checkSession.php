<?php

ini_set('session.cookie_lifetime', 60 * 60 * 72);
ini_set('session.gc-maxlifetime', 60 * 60 * 72);
session_start();
$time = $_SERVER['REQUEST_TIME'];


if (!isset($_SESSION['last_active']) || (isset($_SESSION['last_active']) && ($time - $_SESSION['last_active']) > $_SESSION['timeout_duration'])) {
    session_unset();
    session_destroy();
    header('HTTP/1.1 403 Unauthorized');
    die();
}