<?php

require('checkSession.php');

//Determine privileges
if($_SESSION['userType']!="admin"){
    header('Location: ../chemicalDB.php');
    die();
}

if($_SERVER['REQUEST_METHOD'] === "GET"){

    require '../../connectvars.php';

    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_CHEMICALS);
    if(!$db_connection){
        echo "error connecting to DB!";
        die(mysqli_connect_error());
    }
    mysqli_set_charset ($db_connection, "utf8");

    echo '<table class="table table-bordered" style="table-layout: fixed;" id="chemicals_table">
            <thead>
              <tr>
                <th class="text-center align-top" style="width: 25%;">Chemical</th>
                <th class="text-center align-top" style="min-width: 10%">CAS Number</th>
                <th class="text-center align-top" style="min-width: 15%">GHS Pictograms</th>
                <th class="text-center align-top" style="min-width: 7%;">Quantity</th>
                <th class="text-center align-top" style="width: 10%">SDS</th>
                <th class="text-center align-top" style="width: 10%">SOP</th>
                <th class="text-center align-top" style="min-width: 6%">Location</th>
              </tr>
            </thead>
            <tbody>';
    $query = "SELECT id, chemical_name, company, article_number, CAS_number, weight_quantity, GHS_pictogram_codes, GHS_H_codes, GHS_P_codes, MSDS_EN, MSDS_DE, SOP_EN, SOP_DE, location FROM all_chemicals ORDER BY chemical_name";
    $result = mysqli_query($db_connection, $query);
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
            $CAS_Numbers = explode(",", str_replace(" ", "", $row['CAS_number']));
            $GHS_pictogram_codes = explode("\\", str_replace(" ", "", $row["GHS_pictogram_codes"]));
            $GHS_H_codes = explode("\\", str_replace(" ", "", $row["GHS_H_codes"]));
            $GHS_P_codes = explode("\\", str_replace(" ", "", $row["GHS_P_codes"]));
            echo '<tr>';
            echo '<td class="text-center"><b>'.$row["chemical_name"].'</b><br />'.$row["company"].',<br>'.$row["article_number"].'<div id="chemical_'.$row['id'].'" style="display: none;">'.$row['chemical_name'].'-'.$row['CAS_number'].'</div></td>';
            echo '<td class="text-center">';
            $i = 0;
            foreach ($CAS_Numbers as $key => $value) {
                if(!$value){
                    echo "No CAS Number";
                } else if (++$i == count($CAS_Numbers)) {
                    echo $value;
                } else {
                    echo $value.",<br>";
                }
            }
            echo '</td>';
            echo '<td class="text-center">';
            foreach ($GHS_pictogram_codes as $key => $value) {
                if(!$value){
                    echo "No GHS Pictogram";
                } else {
                    echo '<img class="img-responsive" width="50" src="GHS_symbols/' . $value . '.svg"/>';
                }
            }
            echo '</td>';
            // echo '<td class="text-center">';
            // $i = 0;
            // echo "<b>H:</b>";
            // foreach ($GHS_H_codes as $key => $value) {
            //   if(!$value){
            //     echo "No GHS H phrases/No SDS";
            //   } else if (++$i == count($GHS_H_codes)) {
            //     echo $value;
            //   } else {
            //     echo $value.", ";
            //   }
            // }
            // $i = 0;
            // echo "<br><b>P:</b>";
            // foreach ($GHS_P_codes as $key => $value) {
            //   if(!$value){
            //     echo "No GHS P phrases/No SDS";
            //   } else if (++$i == count($GHS_P_codes)) {
            //     echo $value;
            //   } else {
            //     echo $value.", ";
            //   }
            // }
            // echo '</td>';
            $i = 0;

            echo '<td class="text-center">'.$row["weight_quantity"].'</td>';
            // MSDS
            echo '<td class="text-center">';
            if ($row['MSDS_EN']=="Yes") {
                echo '<a href="#" id="sds_en_'.$row['id'].'" class="sds_button">English</a><br/>';
            } else {
                echo 'No English SDS<br/>';
            }
            if ($row['MSDS_DE']=="Yes") {
                echo '<a href="#" id="sds_de_'.$row['id'].'" class="sds_button">German</a>';
            } else {
                echo 'No German SDS';
            }
            echo '</td>';
            // SOP
            echo '<td class="text-center">';
            if ($row['SOP_EN']=="Yes") {
                echo '<a href="#" id="sop_en_'.$row['id'].'" class="sop_button">English</a><br/>';
            } else {
                echo 'No English SOP<br/>';
            }
            if ($row['SOP_DE']=="Yes") {
                echo '<a href="#" id="sop_de_'.$row['id'].'" class="sop_button">German</a>';
            } else {
                echo 'No German SOP';
            }
            echo '</td>';
            echo '<td class="text-center">'.$row["location"].'</td>';
//            echo '<td class="text-center">
//                <a class="btn btn-sm btn-primary btn-min-margin" href="#" class="view_button" id="view_'.$row['id'].'">View</a><br/>
//                <a class="btn btn-sm btn-success btn-min-margin" href="#" class="edit_button" id="edit_'.$row['id'].'">Edit</a><br/>
//                <a class="btn btn-sm btn-danger btn-min-margin" href="#" class="delete_button" id="delete_'.$row['id'].'">Delete</a>
//              </td>';
            echo '<tr>';
        }
    }
    echo '</tbody>
          </table>';
    mysqli_close($db_connection);
}

